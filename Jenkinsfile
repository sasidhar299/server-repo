def getEnvFromBranch(branch) {
  if (branch == 'master') {
    return 'PROD'
  } else if (branch == 'QA') {
    return 'QA'
  } else {
    return 'DEV'
 }
}

def getApiStageFromBranch(branch) {
  if (branch == 'master') {
    return 'api-PROD'
  } else if (branch == 'QA') {
    return 'api-QA'
  } else {
    return 'api-DEV'
 }
}

pipeline {

  agent any

   tools {
        jdk 'jdk'
        maven 'mvn 3.6.3' 
    }
    // define the required environment variables, which are used later in pipeline
   environment {

        PROJ_TOOL_LOCATION = "${env.WORKSPACE}/ProjTool/Axway-${env.PROJTOOL_VERSION}/apigateway/posix/bin"
        APIGW_MAVEN_PLUGIN_HOME = "${env.WORKSPACE}/MAVEN-PLUGIN/apigw-maven-plugin-${env.MAVEN_PLUGIN_VERSION}"
        POLICY_DIRECTORY = "${env.WORKSPACE}/ApiCode/api-sys-petstoreci/api/policies"
        SERVER_PROJECT     = "${env.WORKSPACE}/server-project/policies"
        DEPLOYMENT_PROJECT = "${env.WORKSPACE}/deployment-project"

        targetedEnv = getEnvFromBranch(env.BRANCH_NAME)
        targetedApiStage = getApiStageFromBranch(env.BRANCH_NAME)

        // This can be nexus3 or nexus2
        NEXUS_VERSION = "nexus3"
        // This can be http or https
        NEXUS_PROTOCOL = "http"
        // Where your Nexus is running
        NEXUS_URL = "34.123.124.156:8081/"
        // Repository where we will upload the artifact
        NEXUS_REPOSITORY = "nexus-api-repo"
        // Jenkins credential id to authenticate to Nexus OSS
        NEXUS_CREDENTIAL_ID = "nexus-admin"
        
        // packaging type
        PACKAGING_TYPE_SERVER = "axsar"
        PACKAGING_TYPE_DEPLOY = "axdar"
    }

  stages{

    // Print Required Env Variables
    stage('Print Variables') {
        when { branch 'DEV' }
        steps {
            echo "Branch Name --> ${env.BRANCH_NAME} " 
            echo "Environment --> ${targetedEnv}"
            echo "ApiStage    --> ${targetedApiStage}"
            echo "credentials Id  --> ${credentialsId}"
        }
    }
/*
    // cleans the workspace before everyrun
    stage('CleanWorkspace') {
        steps {
            deleteDir()
            //TODO: check 
        }
    }
*/
    //Stage to Checkout ProjPack and ProjDeploy tar file into ProjTool directory in workspace
    stage('Checkout ProjPack ProjDeploy') {
        when { branch 'DEV' }
        steps {
            sh 'mkdir -p ProjTool'
            dir("ProjTool")
            {
                echo 'Downloading ProjPack and ProjDeploy from Bit Bucket...'  
                git credentialsId: 'bitbucket-cred', url: 'https://sasidhar-awsdevops@bitbucket.org/sasidhar299/proj-tools.git'
                sh "ls -al"
                sh "tar -xvzf cicd_linux_tools.tar.gz"
            }
        }
    }

    //Stage to download Maven Plugin and also to unzip into jenkins workspace
    stage('Download Maven Plugin') {
        when { branch 'DEV' }
        steps {
            sh 'mkdir -p MAVEN-PLUGIN'
            dir("MAVEN-PLUGIN")
            {
                echo 'Downloading Maven tool from AXWAY github releases...' 
                sh "wget -nc https://github.com/Axway-API-Management-Plus/apigw-maven-plugin/archive/v${MAVEN_PLUGIN_VERSION}.tar.gz"
                sh "tar -xvzf v${MAVEN_PLUGIN_VERSION}.tar.gz"
            }
        }
    }

    //Stage to download Maven Plugin and also to unzip into jenkins workspace
    stage('Install Maven Plugin') {
        when { branch 'DEV' }
        steps {
            dir("${APIGW_MAVEN_PLUGIN_HOME}")
            {
                echo 'Installing Maven Plugin...' 
                sh "mvn clean install"
            }
        }
    }
    
    //Stage to perform mvn install in server project policies directory
    stage('Mvn Install - server project') {
            when { branch 'DEV' }
            steps {
                dir("${SERVER_PROJECT}")
                {
                    echo "***** This step is to perform mvn clean install inside server project policy directory *****"
                    sh "mvn -s settings.xml clean install"
                }
            }
        } 

    //Stage to perform mvn package in deployment project policies directory
    stage('Mvn package - deployment project') {
            when { branch 'DEV' }
            steps {
                dir("${DEPLOYMENT_PROJECT}")
                {
                    echo "***** This step is to perform mvn clean install inside deployment project policy directory *****"
                    sh "mvn -s settings.xml clean package"
                }
            }
        } 

    //Stage to ppublish .fed file to nexus artifactory
    stage("publish fed file to nexus") {
        when { branch 'DEV' }
        steps {
            dir("${DEPLOYMENT_PROJECT}")
            {
            script {
                // Read POM xml file using 'readMavenPom' step , this step 'readMavenPom' is included in: https://plugins.jenkins.io/pipeline-utility-steps
                pom = readMavenPom file: "pom.xml";
                // Find built artifact under target folder
                filesByGlob = findFiles(glob: "target/*.${PACKAGING_TYPE_DEPLOY}");
                // Print some info from the artifact found
                echo "${filesByGlob[0].name} ${filesByGlob[0].path} ${filesByGlob[0].directory} ${filesByGlob[0].length} ${filesByGlob[0].lastModified}"
                // Extract the path from the File found
                artifactPath = filesByGlob[0].path;
                // Assign to a boolean response verifying If the artifact name exists
                artifactExists = fileExists artifactPath;

                if(artifactExists) {
                    echo "*** File: ${artifactPath}, group: ${pom.groupId}, packaging: ${PACKAGING_TYPE_DEPLOY}, version ${pom.version}";

                    nexusArtifactUploader(
                        nexusVersion: NEXUS_VERSION,
                        protocol: NEXUS_PROTOCOL,
                        nexusUrl: NEXUS_URL,
                        groupId: pom.groupId,
                        version: pom.version,
                        repository: NEXUS_REPOSITORY,
                        credentialsId: NEXUS_CREDENTIAL_ID,
                        artifacts: [
                            // Artifact generated such as .jar, .ear and .war files.
                            [artifactId: pom.artifactId,
                            classifier: '',
                            file: artifactPath,
                            type: PACKAGING_TYPE_DEPLOY],

                            // Lets upload the pom.xml file for additional information for Transitive dependencies
                            [artifactId: pom.artifactId,
                            classifier: '',
                            file: "pom.xml",
                            type: "pom"]
                        ]
                    );

                } else {
                    error "*** File: ${artifactPath}, could not be found";
                }
              }
            }
        }
    }

    //*****************************
    //*****    DEPLOYMENT  ********
    //*****************************

    //Stage to perform deployment into API Gateway
    stage('Deploy to API Gateway') {
            steps {
                dir("${DEPLOYMENT_PROJECT}")
                {
                    echo "***** This step is to perform deployment into API Gateway *****"
                    sh "mvn -Daxway.anm.host=api-env -Daxway.anm.port=8090 \
                        -Daxway.anm.user=admin -Daxway.anm.password=changeme \
                        -Daxway.deploy.group=test \
                        -s settings.xml clean apigw:deploy"
                }
            }
        } 
    }
}
//TODO: health checks/unittests to the policies and APIs